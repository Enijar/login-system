<?php

session_start();

$database = 'login-system';

$config = [
    'pdo' => [
        'dsn' => 'mysql:dbname=' . $database . ';host=localhost;charset=utf8',
        'username' => 'root',
        'password' => 'pass1'
    ],
    'debug' => true
];

$db = new PDO($config['pdo']['dsn'], $config['pdo']['username'], $config['pdo']['password']);

require_once 'UserLogin.class.php';

$user = new UserLogin();