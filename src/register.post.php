<?php

require_once 'config.php';

if(!empty($_POST['username']) && !empty($_POST['email_address']) && !empty($_POST['password']) && !empty($_POST['password_repeat'])) {
    if($_POST['password'] === $_POST['password_repeat']) {
        $json = $user->register_user($_POST['username'], $_POST['email_address'], $_POST['password']);
        $response = json_decode($json, true);

        if($response['type'] === 'success') {
            $_SESSION['username'] = $_POST['username'];
        }

        echo $json;
    } else {
        echo json_encode(['type' => 'error', 'message' => 'Passwords don\'t match']);
    }
} else {
    echo json_encode(['type' => 'error', 'message' => 'All fields are required']);
}