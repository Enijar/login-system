<?php

require_once 'config.php';

if(!empty($_POST['username']) && !empty($_POST['password'])) {
    $json = $user->login_user($_POST['username'], $_POST['password']);
    $response = json_decode($json, true);

    if($response['type'] === 'success') {
        $_SESSION['username'] = $_POST['username'];
    }

    echo $json;
} else {
    echo json_encode(['type' => 'error', 'message' => 'All fields required']);
}