<?php

require_once 'config.php';

?>
<!DOCTYPE html>

<html>
    <head>
        <script src="jquery.min.js" type="text/javascript"></script>
        <link rel="stylesheet" href="style.css" type="text/css">
    </head>

    <body>
        <div id="ajax-result">
            <div class="hidden">&nbsp;</div>
        </div>

        <form id="login">
            <div class="spacing">
                <label for="username">Username</label>
                <input id="username" type="text" name="username">
            </div>

            <div class="spacing">
                <label for="password">Password</label>
                <input id="password" type="password" name="password">
            </div>

            <div class="spacing">
                <button id="login" type="submit">Login</button>
            </div>
        </form>

        <script>
            (function() {
                $(document).on('submit', '#login', function(event) {
                    event.preventDefault();

                    $.ajax({
                        type: 'POST',
                        url: 'login.post.php',
                        data: {
                            username: $('#username').val(),
                            password: $('#password').val()
                        },
                        success: function(data) {
                            var response = JSON.parse(data);

                            console.log(response);

                            $('#ajax-result').html('<div class="'+ response.type +'">'+ response.message +'</div>');
                        }
                    });
                });
            }());
        </script>
    </body>
</html>